#include "iostream"
#include "vector"

using namespace std;

class Process
{
	private:
		int arrival;
		int worktime;
		int progress;
		int id;
	
	public:
		int getArrivalTime();
		int getWorkTime();
		int getProgress();
		int getID();
		Process operator++(int);
		Process(int, int, int);
		Process();
		~Process();
};

int Process::getArrivalTime()
{
	return this->arrival;
}

int Process::getWorkTime()
{
	return this->worktime;
}

int Process::getProgress()
{
	return this->progress;
}

int Process::getID()
{
	return this->id;
}

Process::Process(int arr, int wrk, int id)
{
	this->arrival = arr;
	this->worktime = wrk;
	this->progress = 0;
	this->id = id;
}

Process::Process()
{
	this->arrival = 0;
	this->worktime = 0;
	this->progress = 0;
}

Process Process::operator++(int)
{
	this->progress++;
	Process temp = *this;
	
	return temp;
}

void swap(Process &a, Process &b)
{
	Process c = a;
	a = b;
	b = c;
}

Process::~Process()
{
	;
}

int main()
{
	int n;
	
	cout << "Prezentacja dzialania algorytmu FCFS.\nWprowadz liczbe procesow: ";
	cin >> n;
	
	vector<Process> procs(n);
	vector<int> time(n, -1);
	
	for(int i = 0; i < n; i++)
	{
		int a, b;
		cout << "\nProces " << i + 1 << "\n\tCzas nadejscia: ";
		cin >> a;
		cout << "\tCzas wykonywania: ";
		cin >> b;
		
		procs[i] = Process(a, b, i);
	}
	
	cout << "\n";
	
	// FCFS - sortowanie bąbelkowe (sortowanie bez umysłowego wysiłku)
	
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if(procs[i].getArrivalTime() < procs[j].getArrivalTime())
				swap(procs[i], procs[j]);
		}
	}
	
	int curr = 0;
	
	for(int t = 0; t < 1000; t++)
	{
		if(t >= procs[curr].getArrivalTime())
		{
			if(time[curr] == -1)
				time[curr] = t;
				
			procs[curr]++;
			
			cout << "t = " << t << ", wykonywany proces: " << procs[curr].getID()
			<< "\t\t(" << 1. * procs[curr].getProgress() / procs[curr].getWorkTime() * 100 << "%)"
			<< "\n";
			
			if(procs[curr].getWorkTime() == procs[curr].getProgress())
			{
				if(curr < n - 1)
					curr++;
				else
					break;
			}
		}
	}
	
	for(int i = 0; i < n; i++)
	{
		cout << "ID: " << procs[i].getID() << ", Czas nadejscia: " << procs[i].getArrivalTime() 
		<< ", Czas wykonywania: " << procs[i].getWorkTime() << ", Wykonanie: "
		<< time[i] << "\n";
	}
	
	
	return 0;
}
